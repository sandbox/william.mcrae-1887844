<?php

/**
 * @file
 * Provides primary Drupal hook implementations.
 *
 * @author William McRae ("_wdm_", http://drupal.org/user/631972)
 */

/**
 * Implements hook_init().
 */
function composer_autoloader_init() {
  // Check for cached list of vendor dirctories, otherwise rebuild list.
  if ($vendors = cache_get('composer_autoloader')) {
    $vendors = $vendors->data;
  }
  else {
    $vendors = composer_autoloader_cache_rebuild();
  }

  require_once __DIR__ . '/SharedComposerAutoload.php';
  SharedComposerAutoload::getLoader($vendors);
}

/**
 * Rebuild composer vendor directory list cache.
 *
 * @return array
 *   An array of vendor directory paths relative to DRUPAL_ROOT.
 */
function composer_autoloader_cache_rebuild() {
  $vendors = array();

  // Cycle through each of the enabled modules and check for /vendor/autoload.php.
  $modules = module_list();
  foreach ($modules as $module) {
    $path = drupal_get_path('module', $module);
    $vendor_path = $path . '/vendor';
    $autoload_path = $vendor_path . '/autoload.php';
    if (file_exists($autoload_path)) {
      $vendors = array_merge($vendors, array($vendor_path));
    }
  }

  // Cache list of vendor paths and return for immediate use.
  cache_set('composer_autoloader', $vendors);
  return $vendors;
}
