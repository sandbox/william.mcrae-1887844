<?php

/**
 * @file
 * Share autoloader implementation for multiply vendor directories.
 *
 * Base on the composer generated ComposerAutoloaderInit, namespaces and
 * classmap files.
 */


class SharedComposerAutoload {

  /**
   * Create and register a class loader for a list of vendors.
   *
   * The ClassLoader instance from the first vendor directory is used.
   *
   * @param array $vendors
   *   An array of vendor directory paths.
   *
   * @return mixed
   *   A registered class loader or FALSE if not available.
   */
  public static function getLoader($vendors) {
    $loader = FALSE;
    if (!empty($vendors)) {
      require_once $vendors[0] . '/composer/ClassLoader.php';

      $loader = new \Composer\Autoload\ClassLoader();

      foreach ($vendors as $vendor) {
        $map = require $vendor . '/composer/autoload_namespaces.php';
        foreach ($map as $namespace => $path) {
          $loader->add($namespace, $path);
        }

        $class_map = require $vendor . '/composer/autoload_classmap.php';
        if ($class_map) {
          $loader->addClassMap($class_map);
        }
      }

      $loader->register();
    }

    return $loader;
  }
}
